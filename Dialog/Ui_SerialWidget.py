# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'SerialWidget.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_SerialWidget(object):
    def setupUi(self, SerialWidget):
        if not SerialWidget.objectName():
            SerialWidget.setObjectName(u"SerialWidget")
        SerialWidget.resize(521, 324)
        self.gridLayoutWidget = QWidget(SerialWidget)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(9, 9, 501, 301))
        self.settingsLayout = QGridLayout(self.gridLayoutWidget)
        self.settingsLayout.setObjectName(u"settingsLayout")
        self.settingsLayout.setContentsMargins(0, 0, 0, 0)
        self.timeoutLineEdit = QLineEdit(self.gridLayoutWidget)
        self.timeoutLineEdit.setObjectName(u"timeoutLineEdit")

        self.settingsLayout.addWidget(self.timeoutLineEdit, 7, 1, 1, 4)

        self.refreshPortsButton = QPushButton(self.gridLayoutWidget)
        self.refreshPortsButton.setObjectName(u"refreshPortsButton")

        self.settingsLayout.addWidget(self.refreshPortsButton, 0, 1, 1, 1)

        self.flowControlCheckBox = QCheckBox(self.gridLayoutWidget)
        self.flowControlCheckBox.setObjectName(u"flowControlCheckBox")

        self.settingsLayout.addWidget(self.flowControlCheckBox, 5, 1, 1, 3)

        self.terminatorComboBox = QComboBox(self.gridLayoutWidget)
        self.terminatorComboBox.setObjectName(u"terminatorComboBox")

        self.settingsLayout.addWidget(self.terminatorComboBox, 2, 1, 1, 4)

        self.baudRateComboBox = QComboBox(self.gridLayoutWidget)
        self.baudRateComboBox.setObjectName(u"baudRateComboBox")

        self.settingsLayout.addWidget(self.baudRateComboBox, 1, 1, 1, 4)

        self.label_4 = QLabel(self.gridLayoutWidget)
        self.label_4.setObjectName(u"label_4")

        self.settingsLayout.addWidget(self.label_4, 0, 0, 1, 1)

        self.parityLayout = QHBoxLayout()
        self.parityLayout.setObjectName(u"parityLayout")
        self.noneParityCheckBox = QCheckBox(self.gridLayoutWidget)
        self.noneParityCheckBox.setObjectName(u"noneParityCheckBox")

        self.parityLayout.addWidget(self.noneParityCheckBox)

        self.evenParityRadioButton = QRadioButton(self.gridLayoutWidget)
        self.evenParityRadioButton.setObjectName(u"evenParityRadioButton")

        self.parityLayout.addWidget(self.evenParityRadioButton)

        self.oddParityRadioButton = QRadioButton(self.gridLayoutWidget)
        self.oddParityRadioButton.setObjectName(u"oddParityRadioButton")

        self.parityLayout.addWidget(self.oddParityRadioButton)


        self.settingsLayout.addLayout(self.parityLayout, 4, 1, 1, 4)

        self.label_5 = QLabel(self.gridLayoutWidget)
        self.label_5.setObjectName(u"label_5")

        self.settingsLayout.addWidget(self.label_5, 1, 0, 1, 1)

        self.label_8 = QLabel(self.gridLayoutWidget)
        self.label_8.setObjectName(u"label_8")

        self.settingsLayout.addWidget(self.label_8, 4, 0, 1, 1)

        self.label_9 = QLabel(self.gridLayoutWidget)
        self.label_9.setObjectName(u"label_9")

        self.settingsLayout.addWidget(self.label_9, 5, 0, 1, 1)

        self.label_6 = QLabel(self.gridLayoutWidget)
        self.label_6.setObjectName(u"label_6")

        self.settingsLayout.addWidget(self.label_6, 2, 0, 1, 1)

        self.label_7 = QLabel(self.gridLayoutWidget)
        self.label_7.setObjectName(u"label_7")

        self.settingsLayout.addWidget(self.label_7, 3, 0, 1, 1)

        self.suffixComboBox = QComboBox(self.gridLayoutWidget)
        self.suffixComboBox.setObjectName(u"suffixComboBox")

        self.settingsLayout.addWidget(self.suffixComboBox, 3, 1, 1, 4)

        self.portComboBox = QComboBox(self.gridLayoutWidget)
        self.portComboBox.setObjectName(u"portComboBox")

        self.settingsLayout.addWidget(self.portComboBox, 0, 2, 1, 3)

        self.label_10 = QLabel(self.gridLayoutWidget)
        self.label_10.setObjectName(u"label_10")

        self.settingsLayout.addWidget(self.label_10, 6, 0, 1, 1)

        self.removeEchoCheckBox = QCheckBox(self.gridLayoutWidget)
        self.removeEchoCheckBox.setObjectName(u"removeEchoCheckBox")

        self.settingsLayout.addWidget(self.removeEchoCheckBox, 6, 1, 1, 3)

        self.label_11 = QLabel(self.gridLayoutWidget)
        self.label_11.setObjectName(u"label_11")

        self.settingsLayout.addWidget(self.label_11, 7, 0, 1, 1)


        self.retranslateUi(SerialWidget)

        QMetaObject.connectSlotsByName(SerialWidget)
    # setupUi

    def retranslateUi(self, SerialWidget):
        SerialWidget.setWindowTitle(QCoreApplication.translate("SerialWidget", u"Form", None))
        self.refreshPortsButton.setText(QCoreApplication.translate("SerialWidget", u"Refresh", None))
        self.flowControlCheckBox.setText("")
        self.label_4.setText(QCoreApplication.translate("SerialWidget", u"Port", None))
        self.noneParityCheckBox.setText(QCoreApplication.translate("SerialWidget", u"None", None))
        self.evenParityRadioButton.setText(QCoreApplication.translate("SerialWidget", u"Even", None))
        self.oddParityRadioButton.setText(QCoreApplication.translate("SerialWidget", u"Odd", None))
        self.label_5.setText(QCoreApplication.translate("SerialWidget", u"Baudrate", None))
        self.label_8.setText(QCoreApplication.translate("SerialWidget", u"Parity", None))
        self.label_9.setText(QCoreApplication.translate("SerialWidget", u"FlowControl", None))
        self.label_6.setText(QCoreApplication.translate("SerialWidget", u"Terminator", None))
        self.label_7.setText(QCoreApplication.translate("SerialWidget", u"Suffix", None))
        self.label_10.setText(QCoreApplication.translate("SerialWidget", u"RemoveEcho", None))
        self.removeEchoCheckBox.setText("")
        self.label_11.setText(QCoreApplication.translate("SerialWidget", u"Timeout (X s)", None))
    # retranslateUi

