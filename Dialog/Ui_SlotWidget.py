# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'SlotWidget.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_SlotWidget(object):
    def setupUi(self, SlotWidget):
        if not SlotWidget.objectName():
            SlotWidget.setObjectName(u"SlotWidget")
        SlotWidget.resize(611, 60)
        self.gridLayoutWidget = QWidget(SlotWidget)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(10, 10, 591, 41))
        self.slotLayout = QGridLayout(self.gridLayoutWidget)
        self.slotLayout.setObjectName(u"slotLayout")
        self.slotLayout.setSizeConstraint(QLayout.SetMaximumSize)
        self.slotLayout.setContentsMargins(0, 0, 0, 0)
        self.lvSupplyComboBox = QComboBox(self.gridLayoutWidget)
        self.lvSupplyComboBox.setObjectName(u"lvSupplyComboBox")

        self.slotLayout.addWidget(self.lvSupplyComboBox, 0, 1, 2, 1)

        self.arduinoComboBox = QComboBox(self.gridLayoutWidget)
        self.arduinoComboBox.setObjectName(u"arduinoComboBox")

        self.slotLayout.addWidget(self.arduinoComboBox, 0, 3, 2, 1)

        self.slotLabel = QLabel(self.gridLayoutWidget)
        self.slotLabel.setObjectName(u"slotLabel")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.slotLabel.sizePolicy().hasHeightForWidth())
        self.slotLabel.setSizePolicy(sizePolicy)
        self.slotLabel.setMinimumSize(QSize(60, 0))

        self.slotLayout.addWidget(self.slotLabel, 0, 0, 2, 1)

        self.hvSupplyComboBox = QComboBox(self.gridLayoutWidget)
        self.hvSupplyComboBox.setObjectName(u"hvSupplyComboBox")

        self.slotLayout.addWidget(self.hvSupplyComboBox, 0, 2, 2, 1)


        self.retranslateUi(SlotWidget)

        QMetaObject.connectSlotsByName(SlotWidget)
    # setupUi

    def retranslateUi(self, SlotWidget):
        SlotWidget.setWindowTitle(QCoreApplication.translate("SlotWidget", u"Form", None))
        self.slotLabel.setText(QCoreApplication.translate("SlotWidget", u"Slot", None))
    # retranslateUi

