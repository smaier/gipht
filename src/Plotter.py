import matplotlib.pyplot as plt
import yaml


class IvPlot():
    def __init__( self, pSlotOrFile, pFromFile = False ):
        self.ivFig = plt.gcf()
        self.ivFig.canvas.draw()
        if pFromFile:
            #Read file
            u = []
            i = []
            with open(pSlotOrFile, 'r') as f:
                fileContent = yaml.safe_load(f)
                for point in fileContent["Data"]:
                    u.append( float( point["Voltage"] ) )
                    i.append( float( point["Current"] ) )
            self.ivFig.canvas.set_window_title("I(V) Measurement module " + fileContent["Info"]["ID"] )
            self.update( u, i )
        else:
            self.ivFig.canvas.set_window_title("I(V) Measurement module " + pSlotOrFile.module.id)

    def update( self , pV_meas, pI_meas):

        u = []
        i = []
        plt.cla()
        plt.xlabel('Voltage (V)')
        plt.ylabel('Current (A)')
        plt.title("I(V) Measurement")
        plt.grid(True)
        for index in range (len ( pI_meas) ) :
            u.append( pV_meas[index] )
            i.append( pI_meas[index] )

        self.ivFig.show()
        
        plt.plot(u, i, lw=2, color='red')
        self.ivFig.canvas.draw()
    
    def close ( self ):
        self.ivFig.clf()