# Gipht

Graphical user Interface for PHase 2 Tracker objects v2.1

[Here](https://gitlab.cern.ch/smaier/ph2_ot_module_test_manual/-/blob/master/manual.pdf you can find the current manual for the Outer Tracker module test bench and GIPHT.

Installation
```bash
git clone --recurse-submodules -b master https://gitlab.cern.ch/cms_tk_ph2/gipht.git
cd gipht
sh compileSubModules.sh
```
Update
```bash
git pull --recurse-submodules
git update --recursive
sh compileSubModules.sh
```

GIPHT requirements:
For developers:
1. `sudo yum install build-essential qtcreator qt5-default`

For Users:
1. `sudo yum install python3`
2. `sudo python3 -m pip install -U pip`
3. `sudo python3 -m pip install -r requirements.txt`

Start
```bash
python3 gipht.py
```





